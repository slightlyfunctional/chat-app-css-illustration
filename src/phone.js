import delay from 'delay';

document.addEventListener('DOMContentLoaded', async () => {
  const receivedTextMessages = document.querySelectorAll('.phone__text');
  const receivedMedia = document.querySelector('.phone__media');
  const receivedActions = document.querySelectorAll('.input__option');
  const textLoadingMessages = document.querySelectorAll('.text__loading');
  
  for (const textMessage of receivedTextMessages) {
    textMessage.style.visibility = 'hidden';
  }

  receivedMedia.style.visibility = 'hidden';
  
  for (const receivedAction of receivedActions) {
    receivedAction.style.visibility = 'hidden';
    receivedAction.style.opacity = 0;
  }

  for (let i = 0; i < receivedTextMessages.length; i++) {
    await revealNextMessage(receivedTextMessages[i], textLoadingMessages[i], i === 2 ? receivedMedia : null);
  }

  for (const receivedAction of receivedActions) {
    receivedAction.style.visibility = 'visible';
    receivedAction.style.transition = '5s all ease-in-out';
    receivedAction.style.opacity = 1;
    await delay(1000);
  }

  document.querySelector('form[name="phone-input"]').addEventListener('submit', (e) => e.preventDefault());
});

async function revealNextMessage (textNode, loadingNode, mediaNode = null) {
  textNode.style.display = 'none';
  loadingNode.style.display = 'flex';
  await delay(3000);
  textNode.style.visibility = 'visible';
  loadingNode.style.display = 'none';
  if (mediaNode) {
    mediaNode.style.visibility = 'visible';
  }
  textNode.style.display = 'block';
}
