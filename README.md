# Frontend Mentor - Chat app CSS illustration solution

This is a solution to the [Chat app CSS illustration challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/chat-app-css-illustration-O5auMkFqY). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the component depending on their device's screen size
- **Bonus**: See the chat interface animate on the initial load

### Screenshot

![Screenshot](./screenshot.png)

### Links

- Live Site URL: [Chat App CSS Illustration](https://slightlyfunctional.gitlab.io/chat-app-css-illustration)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- [Sass](https://sass-lang.com/) - CSS Preprocessor
- [Parcel](https://parceljs.org/) - JS bundler

### What I learned

I learned more about how CSS animations can liven up a page. I also found getting the background "just right" took a bit of trial and error; admittedly, it still doesn't feel right.

### Useful resources

- [Unicode Symbols](https://unicodeplus.com/U+2329) - I found this resource useful for finding the right angled arrow to best emulate a phone "back" icon. 

## Author

- Website - [slightlyfunctional.com](https://slightlyfunctional.com)
- Frontend Mentor - [@slightlyfunctional](https://www.frontendmentor.io/profile/slightlyfunctional)
